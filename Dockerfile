#
# Docker image for building Android NDK libraries using Conan
#
# @author Jacob Andersen <jacob.andersen@alexandra.dk>

FROM ubuntu:focal
  
ARG NDK_VERSION=r20b

# Avoid tzdata prompt during docker build
ENV TZ=Europe/Copenhagen
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone

RUN apt-get update && apt-get install -y wget unzip cmake python3-pip clang clang-tidy clang-format cppcheck libc++abi-dev libc++-dev && pip3 install conan
RUN wget https://dl.google.com/android/repository/android-ndk-$NDK_VERSION-linux-x86_64.zip && unzip android-ndk-$NDK_VERSION-linux-x86_64.zip && rm android-ndk-$NDK_VERSION-linux-x86_64.zip
ENV ANDROID_NDK_HOME /android-ndk-$NDK_VERSION

# Include bintray as conan remote
RUN conan remote add 4s https://api.bintray.com/conan/4s/4s-phg --insert=0

COPY profiles /root/.conan/profiles 
