
Docker image for building Android NDK libraries using Conan
===========================================================

Documentation
-------------

TBD


Issue tracking
--------------

If you encounter bugs or have a feature request, our issue tracker is
available
[here](https://issuetracker4s.atlassian.net/projects/PM/). Please
read our [general 4S
guidelines](http://4s-online.dk/wiki/doku.php?id=process%3Aoverview)
before using it.


License
-------

The 4SDC library is licensed under the [Apache 2.0
license](http://www.apache.org/licenses/LICENSE-2.0).
